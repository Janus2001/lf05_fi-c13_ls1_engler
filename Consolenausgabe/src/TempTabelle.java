
public class TempTabelle {
	
	public static void main(String[] arg) {
		
		String a = "Fahrenheit";
		String b = "Celsius"; 
		String m = "----------------------";
		
		int c = -20;
		int d = -10; 
		int e = 0;
		int f = 20;
		int g = 30;
		
		double h = -28.8889;
		double i = -23.3333;
		double j = -17.7778;
		double k = -6.6667;
		double l = -1.1111; 
		
		System.out.printf( "%-11s| %9s\n", a , b );
		System.out.printf("%s\n" , m);
		System.out.printf( "%-11d| %9.2f\n" , c , h );
		System.out.printf( "%-11d| %9.2f\n" , d , i );
		System.out.printf( "%+-11d| %9.2f\n" , e , j );
		System.out.printf( "%+-11d| %9.2f\n" , f , k );
		System.out.printf( "%+-11d| %9.2f\n" , g , l );
		System.out.println();
	}

}
