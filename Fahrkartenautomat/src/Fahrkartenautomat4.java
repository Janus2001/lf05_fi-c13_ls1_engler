import java.util.Scanner;

public class Fahrkartenautomat4 {
	
	
	public static void main(String[] args) {
		
		//Variablen 
		double zuZahlenderBetrag = 0;
		double rueckgabebetrag; 
		
		// nach dem Zu zahlenden Betrag wir die Anzahl der Tickets eingegeben
		  // die Anzahl der Karten und der Zu zahlende Betrag wird multipliziert 
		boolean endlosmodus = true; 
		
		while (endlosmodus = true) {
			Scanner tastatur = new Scanner(System.in);
			zuZahlenderBetrag= fahrkartenbestellungErfassen(zuZahlenderBetrag); 
			
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			
			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);
			
			boolean x = true; 
			System.out.println("M�chten sie noch mehr Fahrkarten kaufen?");
			System.out.println("Dr�cke 1 f�r ja und 2 f�r nein");
			System.out.println("");
			
			int eingabe = tastatur.nextInt();
					if (eingabe == 1) {
						x = true;
			}
					else if (eingabe == 2) {
						x = false;
						break;
					}
					else {
						break;
					}
					  System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
			                   "vor Fahrtantritt entwerten zu lassen!\n"+
			                   "Wir w�nschen Ihnen eine gute Fahrt.");
					  System.out.println("");
	}
		 
	  
	       // Geldeinwurf
	       // -----------
		 
	       // Fahrscheinausgabe
	       // -----------------
	   
	       
	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	    
	       
	}
	
	
	//Methodenaufrufe 
		
		public static double fahrkartenbestellungErfassen(double zuzahlenderBetrag) {
			 Scanner tastatur = new Scanner(System.in);
			
			 byte anzahlKarten;
			 int eingabe = 0;
			 boolean pruefe = false;
		
		double[] preise = new double [10];
		
		preise[0] = 2.90;
		preise[1] = 3.30;
		preise[2] = 3.60; 
		preise[3] = 1.90;
		preise[4] = 8.60;
		preise[5] = 9.00;
		preise[6] = 9.60; 
		preise[7] = 23.50;
		preise[8] = 24.30;
		preise[9] = 24.90;
		
		String[] nummern = new String [10];
		nummern[0] = "Einzelfahrschein Berlin AB,         gebe 0 ein: ";
		nummern[1] = "Einzelfahrschein Berlin BC,         gebe 1 ein: ";
		nummern[2] = "Einzelfahrschein Berlin ABC,        gebe 2 ein: "; 
		nummern[3] = "Kurzstrecke,                        gebe 3 ein: ";
		nummern[4] = "Tageskarte Berlin AB,               gebe 4 ein: ";
		nummern[5] = "Tageskarte Berlin BC,               gebe 5 ein: ";
		nummern[6] = "Tageskarte Berlin ABC,              gebe 6 ein: "; 
		nummern[7] = "Kleingruppe-Tageskarte Berlin AB,   gebe 7 ein: ";
		nummern[8] = "Kleingruppen-Tageskarte Berlin BC,  gebe 8 ein: ";
		nummern[9] = "Kleingruppe-Tageskarte Berlin ABC,  gebe 9 ein: ";
	
		for( int j = 0; j < nummern.length-1; j++) {
			System.out.println(nummern[j] + preise[j]);
		}
		System.out.println(" ");
		System.out.println("W�hlen sie Ihre Wunschkarte aus: ");
		
		eingabe = tastatur.nextInt();
		
				
				 System.out.println("Wie viele Fahrkarten wollen sie?: ");
				 anzahlKarten = tastatur.nextByte();
			     zuzahlenderBetrag = preise[eingabe]* anzahlKarten; 
			     
					return zuzahlenderBetrag;
		}
		
		public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
			Scanner tastatur = new Scanner(System.in);
			
			   double rueckgabebetrag = 0;
			   double eingezahlterGesamtbetrag = 0.00;
		       double eingeworfeneM�nze;
		       
		       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		       {
		    	   System.out.printf("Noch zu zahlen: %.2f Euro \n " , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
		    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		    	   eingeworfeneM�nze = tastatur.nextDouble();
		           eingezahlterGesamtbetrag += eingeworfeneM�nze;
		           rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		       }
		
			return rueckgabebetrag;
}
	
		public static void fahrkartenAusgeben() {
				
			 System.out.println("\nFahrschein wird ausgegeben");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
		       System.out.println("\n\n");
}
		
		public static double rueckgeldAusgeben(double rueckgabebetrag) {
			
			double zuZahlenderBetrag = 0;
			
			rueckgabebetrag = rueckgabebetrag - zuZahlenderBetrag;
		       if(rueckgabebetrag > 0.00)
		       {
		    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro \n" , rueckgabebetrag );
		    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

		           while(rueckgabebetrag >= 2.00) // 2 EURO-M�nzen
		           {
		        	  System.out.println("2 EURO");
		        	  rueckgabebetrag -= 2.00;
		           }
		           while(rueckgabebetrag >= 1.00) // 1 EURO-M�nzen
		           {
		        	  System.out.println("1 EURO");
		        	  rueckgabebetrag -= 1.00;
		           }
		           while(rueckgabebetrag >= 0.50) // 50 CENT-M�nzen
		           {
		        	  System.out.println("0,50 CENT");
		        	  rueckgabebetrag -= 0.50;
		           }
		           while(rueckgabebetrag >= 0.20) // 20 CENT-M�nzen
		           {
		        	  System.out.println("0,20 CENT");
		        	  rueckgabebetrag -= 0.20;
		           }
		           while(rueckgabebetrag >= 0.10) // 10 CENT-M�nzen
		           {
		        	  System.out.println("0,10 CENT");
		        	  rueckgabebetrag -= 0.10;
		           }
		           while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
		           {
		        	  System.out.println("0,05 CENT");
		        	  rueckgabebetrag -= 0.05;
		           }
		       }
			return 	rueckgabebetrag;
		
		} 
		
		
		

}

	

