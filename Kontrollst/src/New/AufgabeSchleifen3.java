package New;

public class AufgabeSchleifen3 {

	public static void main(String[] args) {

		for (int i = 1; i <= 200; i++) {
		if (i % 7 == 0) {
			System.out.println("Die Zahl " + i + " ist durch 7 Teilbar.");
		}
		
		else if (i % 4 == 0 && i % 5 != 0) {
			System.out.println("Die Zahl " + i + " ist nicht durch 5, aber durch 4 Teilbar.");
		}
		}
		
		}

	}