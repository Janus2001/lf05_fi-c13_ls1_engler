package New;

import java.util.Scanner;

public class AufgabeNoten {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		int eingabe = 0;
		
		
		System.out.println("Gebe eine Note ein: ");
		eingabe = tastatur.nextInt();
		
		if (eingabe == 1)
			System.out.println("Sehr gut");
		else if (eingabe == 2)
			System.out.println("Gut");
		else if (eingabe == 3)
			System.out.println("Befriedigend");
		else if (eingabe == 4) 
			System.out.println("Ausreichend");
		else if (eingabe == 5)
			System.out.println("Mangelhaft");
		else if (eingabe == 6)
			System.out.println("Ungenügend");
		else
			System.out.println("Fehler");
		
	}

}
