import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[]args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Guten Tag Nutzer, wie ist ihr Name?:");
		
		String Name = myScanner.nextLine();
		
		System.out.print("Guten Tag Nutzer, wie ist ihr Alter?:");
		
		int Alter = myScanner.nextInt();
		
		System.out.print("\n\nIhr Name ist " + Name + " und ihr Alter ist " + Alter + ".");
	}
}
