import java.util.Scanner;

public class AufgabenOperatoren {
	
	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Gebe die erste Zahl ein: ");
		int zahl1 = tastatur.nextInt();
		
		System.out.println("Gebe die zweite Zahl ein: ");
		int zahl2 = tastatur.nextInt();
		
		System.out.println("Gebe den operator ein: ");
		char op = tastatur.next().charAt(0);
		
		int ergebnis = 0; 
		
		if (op == '+') {
			ergebnis = zahl1 + zahl2; 
			System.out.println(ergebnis);
		}
		else if (op == '-') {
			ergebnis = zahl1 - zahl2;
			System.out.println(ergebnis);
		}
		else if (op == '*') {
			ergebnis = zahl1 * zahl2;
			System.out.println(ergebnis);
		}
		else if (op == '/') {
			ergebnis = zahl1 / zahl2;
			System.out.println(ergebnis);
		}
		else {
			System.out.println("Fehler!");
		}
}
	
}
