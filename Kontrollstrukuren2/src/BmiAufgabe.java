import java.util.Scanner;

public class BmiAufgabe {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Gebe dein Gewicht ein: ");
		double kg = tastatur.nextDouble();
		
		System.out.println("Gebe deine Körpergröße ein: ");
		double groesse = tastatur.nextDouble();
		
		System.out.println("Gebe das Geschlecht ein ( m = Männlich und w = Weiblich ): ");
		char geschlecht = tastatur.next().charAt(0);
		
		double ergebnis = kg / (groesse * groesse); 
		
		if (geschlecht == 'm')
		if (ergebnis < 20) {
			System.out.println("Untergewichtig"); 
			}
		else if (ergebnis >= 21 && ergebnis <=25){
			System.out.println("Normalgewichtig");
		}
		else if (ergebnis > 25) {
			System.out.println("Übergewichtig");
		}
		
		if (geschlecht == 'w')
		if (ergebnis < 19) {
			System.out.println("Untergewichtig"); 
			}
		else if (ergebnis >= 19 && ergebnis <=24){
			System.out.println("Normalgewichtig");
		}
		else if (ergebnis > 24) {
			System.out.println("Übergewichtig");
		}
}
}
