import java.util.Scanner;

public class MatrixAufgabe {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);

		System.out.print("Gebe eine Zahl zwischen 2 und 9 ein: ");
		int zahl = tastatur.nextInt();
		int wert = 0;
		
		System.out.println("");
		
		while (wert < 100) {
			
			int dieEins= wert / 10;
			int dieZwei = wert % 10;
		
			if (wert % zahl == 0) 
				System.out.printf("%4s", "*");
			
			else if ( dieEins == zahl || dieZwei == zahl)
				System.out.printf("%4s", "*");
			
			else if ( dieEins + dieZwei == zahl) 
				System.out.printf("%4s", "*");
			
			else 
				System.out.printf("%4d", wert);
			
			wert++;
			
			if (wert % 10 == 0 && wert != 0)
				System.out.println();
	
		}
	}

}
