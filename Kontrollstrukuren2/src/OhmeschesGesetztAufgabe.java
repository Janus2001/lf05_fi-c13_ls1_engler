import java.util.Scanner;

public class OhmeschesGesetztAufgabe {
	
	public static void main(String[] args) {
	
		Scanner janustastatur = new Scanner(System.in);
		char auswahl; 
		double u;
		double r;
		double i; 
		
		System.out.println("Was moechten Sie berechnen (U, I, R): ");
		auswahl = janustastatur.next().charAt(0);
		
		if (auswahl == 'r') {
			System.out.println("Spannung in Volt eingeben: ");
			u = janustastatur.nextDouble();
			System.out.println("Strom in Ampere eingeben: ");
			i = janustastatur.nextDouble();
			r = u / i;
			System.out.printf("R = %.3f Ohm", r);
		}
		else if (auswahl == 'i') {
			System.out.println("Spannung in Volt eingeben: ");
			u = janustastatur.nextDouble();
			System.out.println("Widerstand in Ohm eingeben: ");
			r = janustastatur.nextDouble();
			i = u / r;
			System.out.printf("I = %.3f Ampere", i);
	}
		else if (auswahl == 'u') {
			System.out.println("Strom in Ampere eingeben: ");
			i = janustastatur.nextDouble();
			System.out.println("Widerstand in Ohm eingeben: ");
			r = janustastatur.nextDouble();
			u = i * r;
			System.out.printf("U = %.3f Volt", u);
		}
		}
}
