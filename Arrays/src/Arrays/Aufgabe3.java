package Arrays;

import java.util.Scanner; 

public class Aufgabe3 {

	public static void main(String[] args) {
		
		Scanner janus = new Scanner (System.in);
		
		int[] zahlen = new int[5];
		System.out.println("Gebe 5 Zahlen ein:");
		
		zahlen[4] = janus.nextInt();
		zahlen[3] = janus.nextInt();
		zahlen[2] = janus.nextInt();
		zahlen[1] = janus.nextInt();
		zahlen[0] = janus.nextInt();
		
		for(int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}
	}

}
